// console.log('Hellow world');

// [SECTION] Assignment Operator
    // Basic Assignment Operator (=)
    // The assignment operator assigns the value of the right hand operand to a variable.
    let assignmentNumber = 8;
    console.log('the value of the assignmentNumber variable: ' + assignmentNumber);

// [SECTION] Arithmetic Operations

    let x = 200;
    let y = 18;

    console.log('x: ' + x);
    console.log('y: ' + y);
    console.log('-------')


    // Addition
    let sum = x + y;
    console.log('Result of addition operator: ' + sum);

    // Subtraction
    let difference = x - y;
    console.log('Result of subtraction operator: ' + difference);

    // Multiplication
    let product = x * y;
    console.log('Result of multiplication operator: ' + product);

    // Division
    let quotient = x / y;
    console.log('Result of division operator: ' + quotient);

    // Remainder
    let modulo = x % y;
    console.log('Result of modulo operator: ' + modulo);

// Continuation of assignment operator

// Addition Assignment Operator
    // Syntax: Long method
    // assignmentNumber = assignmentNumber + 2;
    // console.log(assignmentNumber);

    // Short method
    assignmentNumber += 2;
    console.log('Result of Addition Assignment Operator: ' + assignmentNumber);

    // Long method
    // assignmentNumber = assignmentNumber -3;
    // console.log(assignmentNumber);

    // Subtraction Assignment Operator
    assignmentNumber -= 3;
    console.log('Result of Subtraction Operator: ' + assignmentNumber);

    // Multiplication Assignment Operator
    assignmentNumber *= 2;
    console.log('Result of Multiplication Operator: ' + assignmentNumber);

    // Subtraction Assignment Operator
    assignmentNumber /= 2;
    console.log('Result of Division Operator: ' + assignmentNumber);

// [SECTION] PEMDAS (Order of Operations)
// Multiple Operators and Parenthesis

let mdas = 1 + 2 - 3 * 4 / 5;
console.log('Result of mdas operation: ' + mdas);

    /* 
        The operations were done in the following order:
        1. 3 * 4 = 12   |   1 + 2 - 12 / 5
        2. 12 / 5 = 2.4 |   1 + 2 - 2.4
        3. 1 + 2 = 3    |   3 - 2.4
        4. 3 - 2.4 = 0.6
    */

let pemdas = (1+(2-3)) * (4/5);
console.log('Result of pemdas operation: ' + pemdas);

// Hierarchy
// Combinations of multiple arithmetic operators will follow the pemdas rule.

    /* 
        1. parenthesis
        2. exponent
        3. multiplication or division
        4. addition or subtraction

        NOTE: will also follow left to right rule.
    */

// [SECTION] Increment and Decrement
// Increment - add
// Decrement - subtract

// Operators that add or subtract values by 1 and reassign the value of the variable where the increment (++) / decrement (--) was applied.

let z = 1;

// Pre-increment
let increment = ++z;
console.log('Result of pre-increment: ' + increment); /* 2 */
console.log('Result of pre-increment of z: ' + z); /* 2 */

// Post-increment
increment = z++;
console.log('Result of post-increment: ' + increment); /* 2 */
console.log('Result of post-increment of z: ' + z); /* 3 */

// increment = z++;
// console.log('Result of post-increment: ' + increment);
// console.log('Result of post-increment of z: ' + z);

// Pre-decrement
let decrement = --z;
console.log('Result of pre-decrement: ' + decrement); /* 2 */
console.log('Result of pre-decrement of z: ' + z); /* 2 */

// Post-decrement
decrement = z--;
console.log('Result of post-decrement: ' + decrement); /* 2 */
console.log('Result of post-decrement of z: ' + z); /* 1 */

// [SECTION] Type Coercion
// is the automatic conversion of values from one data type to another

    console.log("-----");

    let numA = 10; /* number */
    let numB = '12'; /* string */

    let coercion = numA + numB; /* similar to concatenation */
    console.log(coercion); /* 1012 */
    console.log(typeof coercion); /* string */

    let numC = 10; /* number */
    let numD = true; /* boolean */

        // boolean values are
            // true = 1;
            // false = 0;

    let coercion2 = numC + numD; /* similar to concatenation */
    console.log(coercion2); /* 11 */
    console.log(typeof coercion2); /* number */

    let numX = 16; /* number */
    let numY = 14; /* number */

    let nonCoercion = numX + numY; /* similar to concatenation */
    console.log(nonCoercion); /* 1012 */
    console.log(typeof nonCoercion); /* string */

    let num1 = 1;
    let num2 = false;
    coercion = num1 + num2;
    console.log(coercion);
    console.log(typeof coercion);


// [SECTION] Comparision Operators
// are used to evaluate and compare the left and right operands.
// after evaluation, it returns to a boolean value.

// Equality operator (== - read as 'equal t0')
// Compares the value but not the data type

console.log("-----");
console.log(1 == 1); /* true */
console.log(1 == 2); /* false */
console.log(1 == '1'); /* true */
console.log(0 == false); /* true */ 

let juan = 'juan';
console.log('juan' == 'juan'); /* true */
console.log('juan' == 'Juan'); /* false */ /* equality operator is strict with letter casing. */
console.log(juan == 'juan'); /* true */

// Inequality operator (!= - also read as 'not equal')

console.log("-----");
console.log(1 != 1); /* false */
console.log(1 != 2); /* true */
console.log(1 != '1'); /* false */
console.log(0 != false); /* false */

console.log('juan' != 'juan'); /* false */
console.log('juan' != 'Juan'); /* true */ /* equality operator is strict with letter casing. */
console.log(juan != 'juan'); /* false */

console.log("-----");

// [SECTION] Relational Operators
// some comparison operators to check whether one value is greater or less than the other value.
// it will return to true or false value.

let a = 50;
let b = 60;

// GT or Greater Than Operator (>)

let isGreaterThan = a > b; /* 50 > 60 = false */
console.log(isGreaterThan);

// LT or Less Than Operator (<)

let isLessThan = a < b; /* 50 < 60 = true */
console.log(isLessThan);

// GTE or Greater Than or Equal Operator (>=)

let isGTorEqual = a >= b; /* 50 >= 60 = false */
console.log(isGTorEqual);

// LTE or Less Than or Equal Operator (<=)

let isLTorEqual = a <= b; /* 50 < 60 = true */
console.log(isLTorEqual);

// Forced Coercion
let num = 50;
let numStr = '30';
console.log(num > numStr); /* true */
// forced coercion changes string values to number

let str = 'twenty';
console.log(num >= str);
// since the string is not numeric, the string will not be converted to a number, or a.k.a NaN (Not a Number)

console.log("-----");

// Logical AND Operators

let isLegalAge = true;
let isRegistered = false;

let allRequirementsMet = isLegalAge && isRegistered; /* true */
console.log('Result of Logical AND Operator: ' + allRequirementsMet);
// AND Operator requires all / both are true

// Logical OR Operators
let someRequirementsMet = isLegalAge || isRegistered; /* false */
console.log('Result of Logical OR Operator: ' + someRequirementsMet);
// OR operator requires only 1 true

// Logical NOT Operators
// Returns the opposite value
let someRequirementsNotMet = !isLegalAge; /* false */
console.log('Result of logical NOT Operator: ' + someRequirementsNotMet);